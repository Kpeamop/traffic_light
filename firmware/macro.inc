.macro load_flash2ram
	ldi tmp,@0
    ldi ZL,LOW(@1*2)
    ldi ZH,HIGH(@1*2)
    ldi YL,LOW(@2)
    ldi YH,HIGH(@2)
    rcall flash2ram
.endmacro

.macro ram_load
    ldi XL,LOW(@1)
    ldi XH,HIGH(@1)
    add XL,@2
    ld @0,X
.endmacro