.include "tn2313def.inc"
.include "macro.inc"

.equ DELAY_OPEN   	=90
.equ DELAY_BLINK  	=5
.equ DELAY_PREPARE	=3
.equ DELAY_RED		=17

.equ STAGES=4*2

.equ FIRE_STAGE_1=0
.equ FIRE_STAGE_2=4

.equ TICK_MULTIPLIER=30

.def tmp=r16

.def buttons=r18
.def tick_subcount=r19
.def tick_count=r20
.def last_mask=r21
.def stage_mask=r22
.def stage_interval=r23
.def stage=r24

.dseg
    program:    .byte   STAGES
    interval:   .byte   STAGES

.cseg

    rjmp reset ; Reset Handler
    reti ; External Interrupt0 Handler
    reti ; External Interrupt1 Handler
    reti ; Timer1 Capture Handler
    reti ; Timer1 CompareA Handler
    reti ; Timer1 Overflow Handler
    rjmp irq_timer0 ; Timer0 Overflow Handler
    reti ; USART0 RX Complete Handler
    reti ; USART0,UDR Empty Handler
    reti ; USART0 TX Complete Handler
    reti ; Analog Comparator Handler
    reti ; PCINT0 Handler
    reti ; Timer1 Compare B Handler
    reti ; Timer0 Compare A Handler
    reti ; Timer0 Compare B Handler
    reti ; USI Start Handler
    reti ; USI Overflow Handler
    reti ; EEPROM Ready Handler
    reti ; Watchdog Overflow Handler
    reti ; PCINT1 Handler
    reti ; PCINT2 Handler

data_program:
; 1 side   _   GYR
; 2 side   _GYR
;         b        - blink GREEN
    .db 0b00001100, \
        0b10001100, \
        0b00001001,	\
        0b00011001, \
        0b00100001, \
        0b10100001, \
        0b00001001,	\
        0b00001011

data_interval:
    .db DELAY_OPEN,     \
        DELAY_BLINK,    \
        DELAY_RED,		\
        DELAY_PREPARE,  \
        DELAY_OPEN,     \
        DELAY_BLINK,    \
        DELAY_RED,		\
        DELAY_PREPARE

irq_timer0:
    push tmp
    in tmp,SREG
    push tmp

    dec tick_subcount
    brne _irq_timer0_exit

    inc tick_count
    ldi tick_subcount,TICK_MULTIPLIER

_irq_timer0_exit:
    pop tmp
    out SREG,tmp
    pop tmp

    reti

reset:
    ldi tmp,LOW(RAMEND)
    out SPL,tmp

    ; загрузка маски мерцаний в ОЗУ
    load_flash2ram STAGES,data_program,program

    ; загрузка интервалов мерцаний в ОЗУ
    load_flash2ram STAGES,data_interval,interval

    ; portD переключаем 6 "лап" на вывод
    ldi tmp,0b00111111
    out DDRD,tmp

    ldi tmp,0
    out PORTD,tmp

    ; portB подтягиваем к "1" ноги PB0 & PB1
    ldi tmp,0b00000011
    out PORTB,tmp

    ldi tmp,(0<<CS02)|(1<<CS01)|(1<<CS00)
    out TCCR0,tmp

    in tmp,TIMSK
    ldi tmp,1<<TOIE0
    out TIMSK,tmp

    ldi stage,0
    ldi buttons,0

    rcall tick_reset

    sei

main:
    ; читаем входные данные, "нажатия", PB0 & PB1
    ldi tmp,0b00000011
    in buttons,PINB
    and buttons,tmp
    eor buttons,tmp ; кнопки подтянуты к "1", поэтому "переворачиваем биты"

    breq _main_skip_buttons

    ; если состояние 0
    tst stage
    breq _main_button_tick_reset

    ; если состояние 4
    cpi stage,STAGES/2
    breq _main_button_tick_reset

    ; иначе таймер продолжает работать
    rjmp _main_button_skip_tick_reset

    ; производим сброс таймеров
_main_button_tick_reset:
    rcall tick_reset

_main_button_skip_tick_reset:

    ; обработка нажатия нужной клавиши
    andi buttons,0b00000001
    ldi buttons,0
    brne _main_button_2

_main_button_1:
    ; состояния 5,6 и 7 пропускаем
    cpi stage,STAGES/2+1
    brsh _main_skip_buttons

    ; состояние 4 принудительно переводим в 5
    cpi stage,STAGES/2
    brne _main_fire_stage_1

    inc stage

    rjmp _main_skip_buttons

_main_fire_stage_1:
    ldi stage,FIRE_STAGE_1

    rjmp _main_skip_buttons

_main_button_2:
    cpi stage,STAGES/2
    brsh _main_fire_stage_2

    tst stage
    brne _main_skip_buttons

    inc stage

    rjmp _main_skip_buttons

_main_fire_stage_2:
    ldi stage,FIRE_STAGE_2

_main_skip_buttons:

    ram_load stage_interval,interval,stage

    ; проверка интервала, дошел ли он до заданного значения
    cp tick_count,stage_interval
    brlo _main_trig

    ; если дошел, обнуление интервала и переход на след. состояние
    rcall tick_reset
    inc stage

    ; если текущее состояние дошло до конечного из набора, обнуление и его (зацикливаем)
    cpi stage,STAGES
    brne _main_trig

    ldi stage,0

_main_trig:
    ; читаем и выводим состояние текущей последовательности
    ram_load stage_mask,program,stage

    ; если нет blink, то переход на вывод
    mov tmp,stage_mask
    andi tmp,0b10000000
    breq _main_trig_out_mask

    ; выбор промежутка мерцания - вкл/выкл
    cpi tick_subcount,TICK_MULTIPLIER/2
    brsh _main_trig_set_g

_main_trig_clr_g:
    andi stage_mask,0b00011011
    rjmp _main_trig_out_mask
_main_trig_set_g:
    andi stage_mask,0b00111111
_main_trig_out_mask:
    cp last_mask,stage_mask
    breq _main_trig_exit

    mov last_mask,stage_mask
    out PORTD,stage_mask
_main_trig_exit:

    rjmp main

tick_reset:
    ldi tick_count,0
    ldi tick_subcount,TICK_MULTIPLIER

    ret

; @param r16 кол-во загружаемых байт
; @param Y адрес назначения в SRAM
; @param Z адрес источника FLASH
flash2ram:
    lpm
    st Y+,r0
    adiw ZL,1
    dec tmp
    brne flash2ram

    ret
